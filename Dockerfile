FROM martinhelmich/typo3:8.7

# Install sSMTP for mail support
RUN apt-get -qq update
RUN apt-get install -qq ssmtp vim less cron \
        && apt-get clean \
        && rm -r /var/lib/apt/lists/*

COPY config/php.ini /usr/local/etc/php/
COPY config/php-mail.conf /usr/local/etc/php/conf.d/mail.ini
COPY config/ssmtp.conf /etc/ssmtp/ssmtp.conf

COPY ./docker-php-entrypoint-customized /usr/local/bin/
RUN chmod +x /usr/local/bin/*

ENTRYPOINT ["/usr/local/bin/docker-php-entrypoint-customized"]

CMD ["apache2-foreground"]
